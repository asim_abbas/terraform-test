variable "environment" {
  description = "environment name"
  default     = "dev"
  type        = string
}

variable "project" {
  description = "project tag"
  type        = string
}

variable "name" {
  description = "allows for additional sub-project naming"
  default     = "main"
  type        = string
}

variable "cidr_block" {
  description = "what address block the VPC will use"
  type        = string
}


variable "az_number" {
  # Assign a number to each AZ letter used in our configuration
  default = {
    a = 1
    b = 2
    c = 3
    d = 4
    e = 5
    f = 6
  }
}


