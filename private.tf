# private AWS resources
resource "aws_subnet" "az-private" {
  for_each             = local.zone_ids
  availability_zone_id = each.key
  vpc_id               = aws_vpc.vpc.id
  cidr_block           = cidrsubnet(aws_vpc.vpc.cidr_block, 8, local.az_length + var.az_number[data.aws_availability_zone.names[each.key].name_suffix])
  tags = {
    Name        = "private-${var.environment}-${each.key}"
    terraform   = "true"
    project     = "var.project"
    environment = var.environment
    az          = each.key
  }
}

resource "aws_route_table" "private" {
  for_each = local.zone_ids

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "private ${var.environment}-${each.key}"
    terraform   = "true"
    project     = "var.project"
    environment = var.environment
    az          = each.key
  }
}

resource "aws_route_table_association" "private" {
  for_each       = local.zone_ids
  subnet_id      = aws_subnet.az-private[each.key].id
  route_table_id = aws_route_table.private[each.key].id
}

