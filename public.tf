# public facing AWS resources

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "public-${var.environment}"
    terraform   = "true"
    project     = "vpc-module-test"
    environment = var.environment
  }
}

resource "aws_route" "public_gateway_v4" {
  for_each               = local.zone_ids
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.public_igw.id
  depends_on             = [aws_route_table.public]
}

resource "aws_route_table_association" "public" {
  for_each       = local.zone_ids
  subnet_id      = aws_subnet.az-public[each.key].id
  route_table_id = aws_route_table.public.id
}

resource "aws_internet_gateway" "public_igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name      = "test-${var.environment}"
    terraform = "true"
    project   = "vpc-module-test"
  }
}

resource "aws_subnet" "az-public" {
  # Create one subnet for each given availability zone.
  for_each = local.zone_ids

  # For each subnet, use one of the specified availability zones.
  availability_zone_id = each.key

  # By referencing the aws_vpc.main object, Terraform knows that the subnet
  # must be created only after the VPC is created.
  vpc_id = aws_vpc.vpc.id

  # subnetting
  # see: https://www.terraform.io/docs/configuration/functions/cidrsubnet.html
  # build a v4 /24
  cidr_block = cidrsubnet(aws_vpc.vpc.cidr_block, 8, var.az_number[data.aws_availability_zone.names[each.key].name_suffix])

  tags = {
    Name        = "public-${var.environment}-${each.key}"
    terraform   = "true"
    project     = "var.project"
    az          = each.key
    environment = var.environment
  }
}