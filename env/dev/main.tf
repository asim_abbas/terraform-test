
module "VPC" {
  source = "../../"
  cidr_block = "10.163.0.0/16"
  environment = "development"
  project = "bakcend-new"
  name = "terraform-test"
}
