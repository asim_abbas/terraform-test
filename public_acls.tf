resource "aws_network_acl" "public_tier" {
  vpc_id = aws_vpc.vpc.id
  subnet_ids =  [for s in aws_subnet.az-public:s.id]

  tags = {
    Name = "my-public-acl"
    terraform   = "true"
    project     = "var.projectt"
    environment = var.environment
  }



  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
  }

}
