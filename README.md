# Objective #

As per the requirment the main objective was to build the vpc module along with networks and access contol list using the Terraform.  

### Terraform VPC Module 
This module handles Amazon WebService VPC creation and configuration with Public/Private Network and Network ACLs. This module is meant for use with Terraform > 0.12. 
The module will create the VPC based on the cidr_block we are providing in the var.cidr_block. The public subnets are creating based on local.zone_ids that is affilated to availibity zone, there is only one route table for the public subnets and one internet gateway attached with public subnets. The private subnets are also creating based on the local.zone_ids but each subnet is attached with seperate route table and nothing is attahced wih private subnets. For the public subnets and private submet we are using two acls one for each. 

The subnet creation is dynamic and linked with zones availablity we can restrict it as well availability_zones = ["eu-west-1c", "eu-west-1a","eu-west-1b"]

### Requirments 

You will need the following tools setup and configured:

* AWS authentication 
* Terraform provision a new cluster with human readable language HCL.
* File  vpc.tf:  This file has the resource defined with the configuration
* File public.tf: This file is to setup the public network subnets, internet gateway, route table and subnet assoiction with route table
* File private.tf: This file has the configuration for private network subnets, route table and assocition with route table. These private network subnets are not assoicated with any internet gateway or NAT gateways to send/recive the traffic from outer world
* File private_acls:  This has the network acls for the private network 
* public_acls.tf: This file has the network acls for public network 
* File common.tf: This file is getting the data from aws availablilty zones and based on that creating the creteria  to get the dynamic valus of zones. 
* File output.tf: It has the few output that can useful. 

### Usage

To use this module call this on your main file with variables

``` 

module "VPC" {
  source = "git@bitbucket.org:asim_abbas/terraform-test.git"
  cidr_block = "10.163.0.0/16"
  environment = "development"
  project = "bakcend-new"
  name = "terrform-test"
}

```


Also for pipeline tetsing i created the main file inside the env folder sourcing the local. 



